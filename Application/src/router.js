import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

function loadPage (page) {
  return () => import(/* webpackChunkName: "view-[request]" */ `./pages/${page}/${page}.vue`)
}

export default new Router({
  // mode: 'history', // https://router.vuejs.org/guide/essentials/history-mode.html
  routes: [
    {
      path: '/',
      name: 'home',
      component: loadPage('home') // pages/home.vue
    },
    {
      path: '/sales',
      name: 'sales',
      component: loadPage('sales')
    }
  ]
})
