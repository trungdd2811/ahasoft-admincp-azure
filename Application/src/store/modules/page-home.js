// import data from '../../api/data'
// demo data

const sales_today_data = {
  service: 1500000,
  product: 200000,
  prepaid_card: 2000000,
  prepaid_service: 1000000
}

const booking_today_data = {
  total: 20,
  canceles: 3,
  no_show: 1,
  upcoming_booking: 5
}

const client_today_data = {
  new: 5,
  repeat: 15,
  walk_in: 3
}

const system_notice_data = [
  'Notice for server updates',
  'New functions added',
  'New year holidays'
]

const headquarter_notice_data = [
  'New service added',
  'Notice for technology seminar',
  'New year holidays'
]

const salon_qa_data = [
  'Happy new year greetings',
  'New functions added',
  'New year holidays'
]

// initial state
const state = {
  sales_today_data,
  booking_today_data,
  client_today_data,
  system_notice_data,
  headquarter_notice_data,
  salon_qa_data
}

// getters
const getters = {
  getSalesToday: (state) => {
    return state.sales_today_data
  },
  getBookingToday: (state) => {
    return state.booking_today_data
  },
  getClientToday: (state) => {
    return state.client_today_data
  },
  getSystemNotice: (state) => {
    return state.system_notice_data
  },
  getHeadquarterNotice: (state) => {
    return state.headquarter_notice_data
  },
  getSalonQA: (state) => {
    return state.salon_qa_data
  }
}

// mutations
const mutations = {

}

// actions
const actions = {

}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
