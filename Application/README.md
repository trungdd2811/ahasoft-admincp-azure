# Ahasoft-admincp

## Project setup
```
npm install
```

### Compiles and run eslint
```
npm run lint
```

### Compiles and run eslint & auto fix
```
npm run lintfix
```

### Compiles and hot-reloads for development
```
npm run dev
```

### Compiles and minifies for production
```
npm run prod
```

## Dev process
```
1. access master branch
2. run lint & lintfix (when need)
3. run dev
4. run prod
```

### Project folder structure
```
src/
  components: vue component
  pages: vue pages loaded by router
  router: routers for frond-end
  store: store data & action for ui base vuex
  template: css, js, images, scss for static template
  translate: content for multi language
```

### Make component process
```
1. create store/module for UI: data, state, getter
2. create new-component.vue
3. add to Parent Pages / Component
  - import new-component.vue into, then registry & add tag
  - getdata from Getter in store
  - process data for view
4. translate static title / label
5. make mutation, action base need
```